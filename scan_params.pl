#!/usr/bin/perl -w
use strict;

my $endw = 'N';
#my $genes = 'ORF1a-S-ORF3-E-M-ORF6-ORF7-ORF8-N';
my $genes = 'ORF1a-S-ORF3-E-M-ORF6-ORF7-N';
print "Endweight: $endw\nOrfs: $genes\nOpen/Ext";
for (my $gape = 0;$gape <= 2;$gape += 0.1){
	print "\t$gape";
}
print "\n";
for (my $gapo = 0;$gapo <= 30;$gapo += 1){
	print "$gapo";
	for (my $gape = 0;$gape <= 2;$gape += 0.1){
		system("./make_table_diff_bis.pl 'needle -asequence SEQA -bsequence SEQB -gapopen $gapo -gapextend $gape -endopen 10 -endextend 0.5 -datafile EBLOSUM62 -endweight $endw -brief -stdout Y -auto' 'return POSIX::trunc((ID)/LEN*1000)/10' '$genes' > scan/needle.$genes.$endw.$gapo.$gape.out");
		my $res = `grep '^Average diff' scan/needle.$genes.$endw.$gapo.$gape.out`;
		(my $diff) = ($res =~ /^Average diff = ([0-9\.]+)/);
		print "\t$diff";
	}
	print "\n";
}
print "\n";

