#!/usr/bin/perl -w
use strict;
use Math::Round qw(:all);
use String::Pad qw(pad);
use Data::UUID;
my $ugo    = Data::UUID->new;
my $ug = '/tmp/drastic/'.$ugo->create_str();
mkdir '/tmp/drastic' unless -d '/tmp/drastic';
my $aligner = shift @ARGV;
my $perc_formula = shift @ARGV;
my $orf_string = shift @ARGV;
die "Please provide alignment method and percentage formula and orf string, eg: \n./make_table_diff_bis.pl \"needle -asequence SEQA -bsequence SEQB -gapopen 10 -gapextend 0.5 -endopen 10 -endextend 0.5 -datafile EBLOSUM80 -endweight N -brief -stdout Y -auto \" 'return POSIX::trunc((ID)/LENAL*1000)/10' 'ORF1a-S1-ORF3-E-M-ORF6-ORF7-ORF8-N'\n" unless ($aligner && $perc_formula && $orf_string);
my @orfs = split(/\-/,$orf_string);
#my @orfs = qw\ORF1a S ORF3 E M ORF6 ORF7 ORF8 N\;
#my @orfs = qw\ORF1a S ORF3 E M ORF6 ORF7 N\;
#my @orfs = qw\S ORF3 E M ORF6 ORF7 N\;
#my @orfs = qw\ORF1a\;
#my @orfs = qw\S1\;
print "Align: $aligner\nPerc: $perc_formula\nOrfs: $orf_string\n";

open (IN, 'Yu_2019_table3.2_bis.tsv');
my $head = <IN>;chomp($head);
my @prots = split(/\t/,$head);
my @isolates;
my (%yu,%idm,%inputmat);
my $i=1;
while(<IN>){
	chomp;
	my @cols = split;
	$i++;
	$isolates[$i]=shift @cols;
	my $g=0;
	foreach my $el (@cols){
		$g++;
		$yu{lc($prots[$g])}[$i] = $el; #warn "$g $i=$isolates[$i] $el";
	}
}
close IN;
$isolates[17]='Wuhan-Hu-1';
$isolates[18]='CoVZXC21';
print pad(shift(@prots),21,'r','.')."\t";
foreach my $orf (@prots){
	print $orf."\t";
}
print "\n";
for ($i=2;$i<=16;$i++){
	print pad("$i:$isolates[$i]",21,'r','.')."\t";
	foreach my $orf (@prots){
		print $yu{lc($orf)}[$i]."\t";
	}
	print "\n";
}

foreach my $orfu (@orfs){
	#($basename) = ($file =~ /^(.+)\.\S+$/);
	my $orf=lc($orfu);
	#print "Doing $orf\n";
	my $idfile = `cat $orf.ids`;
	chomp($idfile);
	my @ids = split(/\n/ , $idfile);
	#print "@ids\n========\n";
	my %ord;
	my $i=0;
	foreach my $el (@ids){
		$i++;
		$ord{$el} = $i;
	}
	#The spliting only needs to be done once & for all
	system("seqkit split -i $orf.faa"); 
	for ($i=1;$i<=17;$i++){
		#print "\t '$ids[$i]'\t";
		if($ids[$i] =~/\S+/){
			my $len0 = "seqkit stat $orf.faa.split/$orf.id_". $ids[0].".?.faa";
			$len0 = `$len0`;$len0 =~ s/,//g;
			my $leni = "seqkit stat $orf.faa.split/$orf.id_".$ids[$i].".?.faa";
			$leni = `$leni`;$leni =~ s/,//g;
			($len0) = ($len0 =~ /(\d+)$/);
			($leni) = ($leni =~ /(\d+)$/);
			#system("cat $orf.faa.split/$orf.id_".$ids[0].".?.faa $orf.faa.split/$orf.id_".$ids[$i].".?.faa > $ug.faa");
			#system("clustalw -infile=$ug.faa -pim -type=PROTEIN -stats=$orf.clustal_stats > $orf.$ids[$i].scores");
			#system("clustalo -infile=$ug.faa -o $ug.aln > $orf.$ids[$i].scores");
			#system("mafft --globalpair --maxiterate 1000 $ug.faa > $ug.aln");
			#system("mafft --localpair --maxiterate 1000 $ug.faa > $ug.aln");
			#system("mafft --auto --maxiterate 1000 $ug.faa > $ug.aln");
			#system("muscle -in $ug.faa  > $ug.aln");
			#system("blastp -query $orf.faa.split/$orf.id_".$ids[0].".?.faa -subject $orf.faa.split/$orf.id_".$ids[$i].".?.faa -outfmt 6  > $ug.aln");
			#system("needle -asequence $orf.faa.split/$orf.id_".$ids[0].".?.faa -bsequence $orf.faa.split/$orf.id_".$ids[$i].".?.faa -gapopen 11 -gapextend 2 -brief -stdout TRUE -auto > $ug.aln");
			#system("needle -asequence $orf.faa.split/$orf.id_".$ids[0].".?.faa -bsequence $orf.faa.split/$orf.id_".$ids[$i].".?.faa -gapopen 10 -gapextend 0.5 -endopen 10 -endextend 0.5 -datafile EBLOSUM62 -endweight N -brief -stdout Y -auto > $ug.aln");
			my $seqa = "$orf.faa.split/$orf.id_".$ids[0].".?.faa";
			my $seqb = "$orf.faa.split/$orf.id_".$ids[$i].".?.faa";
			my $loc_ali = $aligner;
			$loc_ali =~ s/SEQA/$seqa/;
			$loc_ali =~ s/SEQB/$seqb/;
			system("$loc_ali > $ug.aln");
			if($aligner =~ /^(needle|water)/){#NEEDLE
				# Identity:     945/1260 (75.0%)
				my $needle = `grep "^# Identity:" $ug.aln`;
				my ($id,$len,$perc) = ($needle =~ /^# Identity:\s+(\d+)\/(\d+)\s+\(\s*([0-9\.]+)%/);
				# Similarity:  1116/1273 (87.7%)
				#$needle = `grep "^# Similarity:" $ug.aln`;
				#($id,$len,$perc) = ($needle =~ /^# Similarity:\s+(\d+)\/(\d+)\s+\(\s*([0-9\.]+)%/);
				# Gaps:          22/1273 ( 1.7%)
				$needle = `grep "^# Gaps:" $ug.aln`;
				my ($gid,$glen,$gperc) = ($needle =~ /^#\s+Gaps:\s+(\d+)\/(\d+)\s+\(\s*([0-9\.]+)%/);
				my %indels;
				my @lines = split(/\n/,`cat $ug.aln`);
				foreach my $line (@lines){
				#AAP41047.1         1 MSDNGPQSNQRSAPRITFGGPTDSTDNNQNGGRNGARPKQRRPQGLPNNT     50
				#                     ||||||| ||.||||||||||:||:||:|||.||||||||||||||||||
				#AVP78049.1         1 MSDNGPQ-NQSSAPRITFGGPSDSSDNSQNGERNGARPKQRRPQGLPNNT     49
					if(my ($ac,$seq) = ($line =~ /^(\S+)\.\d+\s+\d+\s+(\S+)\s+/) ){
						$seq =~ s/\s1\s+\-+//g;#remove N-ter end gaps
						$seq =~ s/\-+\s+$len//g;#remove C-ter end gaps
						my $gaps = ($seq =~ s/\-//g);
						
						$indels{$ac} += $gaps;
					}
				}
				my $gap0 = (defined($indels{$ids[0]})?$indels{$ids[0]}:0);
				my $gapi = (defined($indels{$ids[$i]})?$indels{$ids[$i]}:0);
				#$idm{$orf}[$ord{$ids[$i]}]=$perc;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id- ($gap0 > $gapi ? $gap0 : $gapi ) ) / $leni * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id- ($len0 < $leni ? $gap0 : $gapi ) ) / ( $len0 < $leni ? $len0 : $leni ) * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id- ($len0 > $leni ? $gap0 : $gapi ) ) / ( $len0 > $leni ? $len0 : $leni ) * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id- ($gap0 + $gapi )/2 ) / ( $len0 > $leni ? $len0 : $leni ) * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id- ($gap0 + $gapi )/2 ) / $len * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id - $gap0 - $gapi     ) / $len * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id - $gap0  )  / $len  * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id - $gap0  )  / $len0 * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id - $gap0  )  / $leni * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id - $gapi  ) / $leni * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id - $gapi  ) / $len0 * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id - $gapi  ) / $len  * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc(($id/$len0 +$id/$leni)/2*1000)/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc(($id)/($len-($gap0 + $gapi))*1000)/10;#Average diff = 0.7 (73.5/105)
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id ) / ( $len0 < $leni ? $len0 : $leni ) * 1000 )/10;#Average diff = 0.321 (33.7/105)
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc( ($id ) / ( ($len0+$gap0) < ($leni+$gapi) ? $len0+$gap0 : $leni+$gapi ) * 1000 )/10;
				#$idm{$orf}[$ord{$ids[$i]}]=POSIX::trunc(($id)/$len*1000)/10;#Average diff = 0.034 (3.6/105)
				my $loc_perc = $perc_formula;
				$loc_perc =~ s/ID/$id/g;
				$loc_perc =~ s/LENAL/$len/g;
				$loc_perc =~ s/LENA/$len0/g;
				$loc_perc =~ s/LENB/$leni/g;
				$loc_perc =~ s/GAPA/$gap0/g;
				$loc_perc =~ s/GAPB/$gapi/g;
				$idm{$orf}[$ord{$ids[$i]}]=eval $loc_perc;
				#print "$len0|$leni|$len|$id|$gap0|$gapi => $idm{$orf}[$ord{$ids[$i]}] \n";
				
				$inputmat{$orf}[$ord{$ids[$i]}]="$len0|$leni|$len|$id|$gap0|$gapi";
				
				#print " : ($id/$len0 +$id/$leni)/2 =".$idm{$orf}[$ord{$ids[$i]}];
			}
			elsif($aligner =~ /^(blast)/){#BLAST
				my $blast = `cat $ug.aln`;
				my ($id1,$id2,$perc) = ($blast =~ /^(\S+)\.\d+\s+(\S+)\.\d+\s+([0-9\.]+)/);
				$idm{$orf}[$ord{$id2}]=POSIX::trunc($perc*10)/10;
				#print " : $perc";
			}else{
				system("t_coffee -other_pg seq_reformat -in $ug.aln -output sim > $orf.$ids[$i].pim");
				system("grep \"^TOP.*".$ids[0]."\" $orf.$ids[$i].pim > $orf.$ids[$i].tor2_pim");
				open (IN, "$orf.$ids[$i].tor2_pim");
				while (<IN>){
					my ($id1,$id2,$perc) = (/(\S+)\.\d+\s+(\S+)\.\d+\s+([0-9\.]+)$/);
					my $mid;
					if($id1 eq $ids[0]){$mid = $id2}elsif($id2 eq $ids[0]){$mid = $id1}else{die "Arghhh : $_ $id1,$id2,$perc"}
					$idm{$orf}[$ord{$mid}]=$perc;
					#print " : $perc";
				}
				close IN;
			}
		}
		#print "\n";
	}
	#last;
}
print "\n".pad('Alignment_data',21,'r','.')."\t";
foreach my $orf (@orfs){
	print $orf."\t";
}
print "\n";
for ($i=2;$i<=18;$i++){
	print pad("$i:$isolates[$i]",21,'r','.')."\t";
	foreach my $orf (@orfs){
		print ((defined $inputmat{lc($orf)}[$i]?$inputmat{lc($orf)}[$i]:'')."\t");
	}
	print "\n";
}

print "\n".pad('Calculated_identities',21,'r','.')."\t";
foreach my $orf (@orfs){
	print $orf."\t";
}
print "\n";
for ($i=2;$i<=18;$i++){
	print pad("$i:$isolates[$i]",21,'r','.')."\t";
	foreach my $orf (@orfs){
		#$perc = nearest(.1,$idm{lc($orf)}[$i]);
		my $perc = $idm{lc($orf)}[$i];
		print (defined($perc) > 0 ? "$perc\t":"\t");
	}
	print "\n";
}

print "\n".pad('CalcId-Yu2019',21,'r','.')."\t";
foreach my $orf (@orfs){
	print $orf."\t";
}
print "\n";
my $tot = my $nb = 0;
for ($i=2;$i<=16;$i++){
	print pad("$i:$isolates[$i]",21,'r','.')."\t";
	foreach my $orf (@orfs){
		if ( $yu{lc($orf)}[$i] ne 'NA'){
			if (defined $idm{lc($orf)}[$i] && $yu{lc($orf)}[$i]) {
				my $diff = nearest(.01, $idm{lc($orf)}[$i]-$yu{lc($orf)}[$i]);
				print "$diff\t";
				$tot += abs($diff);
				$nb++;
			}else{
				print "\t";
			}
		}else{
			print "\t";
		}
	}
	print "\n";
}
print "\nAverage diff = ".nearest(0.001,$tot/$nb)." ($tot/$nb)\n";

